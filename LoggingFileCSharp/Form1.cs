﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoggingFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("Start Form1 Load!");
            //try
            //{
            //    throw new Exception("This is test message...");
            //}
            //catch (Exception ex)
            //{
            //    log.Error(ex.Message);
            //}
            //Console.Read();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            log.Info("Data berhasil disimpan ke LogFile!");
            Console.WriteLine("Button Info Klik");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                throw new Exception("This is test message exception...");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            Console.Read();
        }

        private void button3_Click(object sender, EventArgs e)
        {
        
            // Create a secondary thread by passing a ThreadStart delegate  
            //Thread workerThread = new Thread(new ThreadStart(Print));  
            Thread workerThread = new Thread(delegate()
            {
                try
                {
                    Print();
                    log.Info("Running try");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    log.Error(ex.Message);
                }
            });  
        
            // Start secondary thread  
            workerThread.Start();  
  
            // Main thread : Print 1 to 10 every 0.2 second.   
            // Thread.Sleep method is responsible for making the current thread sleep  
            // in milliseconds. During its sleep, a thread does nothing.  
            for (int i=0; i< 10; i++)  
            {  
                Console.WriteLine("Main thread: {0}", i);  
                Thread.Sleep(200);  
            }         
  
            Console.Read();
        }

        static void Print()  
        {
            throw new Exception("This is message exception!!");
            //for (int i = 11; i < 20; i++)  
            //{  
            //    Console.WriteLine("Worker thread: {0}", i);  
            //    Thread.Sleep(1000);  
            //}
        }
    }
}
