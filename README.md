# Logging using File

- Download Log4Net using nuget.org

- If use Visual Studio 2013, setting nuget.org if failed download using this script :
```
[Net.ServicePointManager]::SecurityProtocol = [Net.ServicePointManager]::SecurityProtocol -bOR [Net.SecurityProtocolType]::Tls12
```

- Add this script in AssemblyInfo.cs
```
...
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
```

- Add this in .config
```
<configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
  </configSections>

  <log4net>
    <appender name="LogFileAppender" type="log4net.Appender.RollingFileAppender">
      <param name="File" value="C:\\LoggingFile\\LogFile.log"/>
      <appendToFile value="true" />
      <!-- <lockingModel type="log4net.Appender.FileAppender+MinimalLock" /> 
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="1MB" />
      <staticLogFileName value="true" /> -->
      <rollingStyle value="Date" />
      <layout type="log4net.Layout.PatternLayout">        
        <conversionPattern value="%date [%thread] %level %logger - %message%newline" />
      </layout>
    </appender>
    <root>
      <level value="ALL" />
      <appender-ref ref="LogFileAppender" />
    </root>
  </log4net>

  <startup> 
	<supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5" />
  </startup>
```

- In Form1.cs add static variable :
```
    static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
```

- Using log file like this, in script you want add into log :
```
log.Info("Data berhasil disimpan ke LogFile!");
log.Error(e.Message);
...
```

# Environment
- Visual Studio 2013 Community
- .Net Framework 4.5.2

# Author
*[Iwan Prasetiyo](mailto:iwan.webdeveloper@gmail.com)*



